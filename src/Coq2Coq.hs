module Coq2Coq (coq2coq, coq2coqE, iOmap) where

import Coq
import Data.Maybe
import Control.Monad.State

coq2coq :: Statement -> Statement
coq2coq = semap coq2coqE

coq2coqE :: Expr -> Expr
coq2coqE = emap map_types

map_types :: Expr -> Expr
map_types t =
  let plains = [("()", "unit"), ("[]", "list"), ("Bool", "bool"), ("Maybe", "option"), ("Word8", "Ascii.ascii"), ("ByteString", "string"), ("Either", "sum"),
  {-TODO: these types are not great at approximating the haskell side-}("Int", "nat"), ("Integer", "nat"), ("Number", "nat")] in
  let types = map (\ (fst', snd') -> (Term fst', Term snd')) plains in
  let types' = (Term "String", Arged [Term "list", Term "Char"]):types in
  foldr (\ (f, t') ty -> replace f t' ty) t types'

iOmap :: Ctor -> Ctor
iOmap orig@(Ctor n nel) =
  let el = map snd nel :: [Maybe Expr] in
  let prepostrenamer = stemap
               (\e -> case e of
                        Arged [Term "IO", t] -> do
                          x <- get
                          put (x + 1)
                          return $ Arged [
                            Term "IO'",
                            Term $ "pre" ++ showcn x,
                            t,
                            Term $ "post" ++ showcn x]
                        x -> return x) :: Expr -> State Integer Expr in
  let pprenappM = fmap prepostrenamer :: Maybe Expr -> Maybe (State Integer Expr) in
  let pprencommSM = sequence . pprenappM :: Maybe Expr -> State Integer (Maybe Expr) in
  let pprenappL = map pprencommSM :: [Maybe Expr] -> [State Integer (Maybe Expr)] in
  let pprencommSL = sequence . pprenappL :: [Maybe Expr] -> State Integer [Maybe Expr] in
  let (newe', ioc) = runState (pprencommSL el) 0 in
  let newe = map (\ (e, (on, _)) -> (on, e)) $ zip newe' nel in
  if nel == newe
  then orig
  else
    let prepost = genPrePost ioc in
    let (named, rest) = span (isJust . fst) newe in
    Ctor n $ named ++ prepost ++ rest
  where showcn 0 = ""
        showcn x = show (x - 1)
        genPrePost 0 = []
        genPrePost x = genPrePost (x - 1) ++ [(Just ("pre" ++ showcn (x - 1)), Nothing), (Just ("post" ++ showcn (x - 1)), Nothing)]
