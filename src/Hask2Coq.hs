{-# LANGUAGE CPP #-}

module Hask2Coq (
  h2c, h2a,
  ht2ct, tc2t,
  genericsIntro,
  isIOtype, iORes,
) where

import qualified Hask as H
import qualified Coq as C

import qualified Data.Char as Ch
import qualified Data.List as L

------ Typeclass mapping ------
tc2t' :: H.Typeclass -> H.Type -> Either String H.Type
tc2t' (H.Typeclass "Show" [a]) t = Right $ H.Fun (H.Fun (H.Plain a) (H.Plain "String")) t
tc2t' (H.Typeclass "Exception" [a]) t = Right $ H.Fun (H.Fun (H.Plain a) (H.Plain "String")) t
tc2t' (H.Typeclass "Read" [a]) t = Right $ H.Fun (H.Fun (H.Plain "String") (H.Plain a)) t
tc2t' (H.Typeclass n _) _ = Left n

tc2t :: [H.Typeclass] -> H.Type -> Either String H.Type
tc2t [] ty = Right ty
tc2t [H.Typeclass "Eq" [a], H.Typeclass "Num" [b]] ty | a == b = Right $ H.tmap (\t -> if t == H.Plain a then H.Plain "Z" else t) ty
tc2t (h:l) t = foldr (\e a -> a >>= (tc2t' e)) (tc2t' h t) l

------ Haskell IO decl to action param decl -----
h2a :: C.Expr -> H.Decl -> C.Ctor
h2a actionType d@(H.Decl dn tcl t) =
  case tc2t tcl t of
    Right t' ->
      let ct' = ht2ct t' in
      let values = case ct' of {
          C.Fun _ _ -> map (\x -> case x of { Left (_, e) -> e; Right (Left _) -> C.Term "_"; Right (Right e) -> e }) $ init $ C.args $ C.replaceBottom actionType ct';
          C.Arged [C.Term "IO", _] -> [];
          x -> error ("unexpected value: " ++ show x)
      } in
      let generics_ = genericsIntroa t' ct' in
      let genericslift = map (\ (n, mt) -> (Just n, mt)) generics_ in
      let argslift = map (\v -> (Nothing, Just v)) values in
      C.Ctor (
        --"(*" ++ __FILE__ ++ ":" ++ show __LINE__ ++ ":" ++ show d ++ "\n" ++ show t' ++ "*)\n" ++ 
        dn ++ "_action") (genericslift ++ argslift)
    Left msg -> error $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ ": Not supported: " ++ msg ++ " in " ++ show d

------ Haskell IO decl to action pattern match -----
isIOtype :: H.Type -> Bool
isIOtype (H.Arged [H.Plain "IO", _]) = True
isIOtype (H.Fun _ t) = isIOtype t
isIOtype _ = False

getIOwrapped :: H.Type -> Maybe H.Type
getIOwrapped (H.Arged [H.Plain "IO", t]) = Just t
getIOwrapped _ = Nothing

iORes :: H.Type -> Maybe H.Type
iORes t = getIOwrapped $ H.getBottom t

------ convert Haskell elems into Coq statements ----
h2c :: H.HS -> [C.Statement]
h2c x@(H.HSDecl (H.Decl dn tcl t)) =
  case tc2t tcl t of
    Right tx -> let res = C.Axiom dn $ genericsIntro tx $ ht2ct tx in [
      --C.Comment $ __FILE__ ++ ":" ++ show __LINE__ ++ ":" ++ show res,
        res
      , C.ExtractConstant dn dn
      ]
    Left msg ->  [C.Comment $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ ": Not supported: " ++ msg ++ " in " ++ show x]
h2c (H.HSData x@(H.Data _ _ [])) = [C.Comment $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ ": Not supported: " ++ show x]
h2c (H.HSData x@(H.Data dn params c)) =
  let newparamnames = map (map Ch.toUpper) params in
  let (H.HSData (H.Data _ _ newc)) = foldr (\(oldname, newname) x' -> H.renamehs oldname newname x') (H.HSData x) $ zip params newparamnames in
  let args = map (\n -> (n, Just $ C.Term "Set")) newparamnames in
  let ctors = map (hc2cc dn) newc in
  let res = C.Inductive dn args ctors in
  [ --C.Comment (__FILE__ ++ ":" ++ show __LINE__ ++ ":" ++ show x),
    --C.Comment (__FILE__ ++ ":" ++ show __LINE__ ++ ":" ++ show res),
    res
  , C.ExtractInductive dn (map (\(H.Ctor s _) -> s) c) Nothing]
h2c (H.HSType (H.Type2 n _ d)) = [C.Definition n (genericsIntro d $ ht2ct d)]
h2c (H.HSKind (H.Kind name arity)) = [
     C.Axiom name (iterate_ arity (\t _ -> C.Fun (C.Term "Set") t) (C.Term "Set"))
   , C.ExtractConstant
       (iterate_ arity (\c i -> c ++ " \"a" ++ show (arity - i) ++ "\"") name)
       (iterate_ arity (\h i -> h ++ " a" ++ show (arity - i)) name)]
   where
     iterate_ 0 _ a = a
     iterate_ n f a = iterate_ (n - 1) f (f a n)

ht2ct :: H.Type -> C.Expr
ht2ct (H.Plain s) = C.Term s
ht2ct (H.Arged []) = error $ __FILE__ ++ ":" ++ show (__LINE__ :: Int) ++ ": Not supported: empty arged"
ht2ct (H.Arged l@(_:_)) = C.Arged $ map ht2ct l
ht2ct (H.Tuple a) = C.Tuple $ map ht2ct a
ht2ct (H.Fun td tcd) = C.Fun (ht2ct td) (ht2ct tcd)

hc2cc :: String -> H.Ctor -> C.Ctor
hc2cc t (H.Ctor n []) = C.Ctor (if n == t then n ++ "'" else n) []
hc2cc t (H.Ctor n cl) =
  let ctl = (map (ht2ct . snd) cl) in
  let gce = map (genericsIntro' (concat $ map (generics . snd) cl)) ctl in
  let gctl = map (\ (ge, (an, _)) -> (an, Just ge)) $ zip gce cl in
  C.Ctor (if n == t then n ++ "'" else n) gctl

--------- explicitize implict [a]-s -------------
generics' :: H.Type -> [String]
generics' (H.Plain s) = if all (\c -> c == '\'' || Ch.isLower c || Ch.isNumber c) s then [s] else []
generics' (H.Arged l) = concat $ map generics (tail l)
generics' (H.Tuple l) = concat $ map generics l
generics' (H.Fun d cd) = generics d ++ generics cd
generics :: H.Type -> [String]
generics = L.nub . generics'

genericsIntro'' :: [String] -> C.Expr -> [(String, Maybe C.Expr)]
genericsIntro'' tv _ = if tv == [] then [] else (map (\n -> (n, typeOf n)) tv)
  where typeOf ('p':'r':'e':_) = Nothing
        typeOf ('p':'o':'s':'t':_) = Nothing
        typeOf _ = Just $ C.Term "Set"
genericsIntro' :: [String] -> C.Expr -> C.Expr
genericsIntro' [] ce = ce
genericsIntro' tv ce = C.Forall (genericsIntro'' tv ce) ce
genericsIntro :: H.Type -> C.Expr -> C.Expr
genericsIntro ht ce = genericsIntro' (generics ht) ce
genericsIntroa :: H.Type -> C.Expr -> [(String, Maybe C.Expr)]
genericsIntroa ht ce = genericsIntro'' (generics ht) ce
